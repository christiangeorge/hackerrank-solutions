﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HackerRankTests
{
    class Warmpup
    {
        //Simple Array Sum
        static int simpleArraySum(int[] ar)
        {
            /*
             * Write your code here.
             */
            var sum = 0;
            foreach (var i in ar)
            {
                sum += i;
            }
            return sum;
        }

        //Compare the Triplets
        static List<int> compareTriplets(List<int> a, List<int> b)
        {
            var results = new int[] { 0, 0 };
            for (var i = 0; i < 3; i++)
            {
                if (a[i] > b[i])
                {
                    results[0]++;
                }
                if (a[i] < b[i])
                {
                    results[1]++;
                }
            }
            return new List<int>(results);
        }

        //A Very Big Sum
        static long aVeryBigSum(long[] ar)
        {
            long result = 0;
            foreach (long l in ar)
            {
                result += l;
            }
            return result;
        }

        //Diagonal Difference
        static int diagonalDifference(int[][] arr)
        {
            var lrSum = 0;
            var rlSum = 0;
            var rlPosition = arr.Length - 1;
            for (var i = 0; i < arr.Length; i++)
            {
                lrSum += arr[i][i];
                rlSum += arr[i][rlPosition];
                rlPosition--;
            }
            return Math.Abs(lrSum - rlSum);
        }

        //Plus Minus
        static void plusMinus(int[] arr)
        {
            var arLength = arr.Length;
            var posCount = 0;
            var negCount = 0;
            var zeroCount = 0;
            for (var i = 0; i < arLength; i++)
            {
                if (arr[i] > 0) posCount++;
                if (arr[i] < 0) negCount++;
                if (arr[i] == 0) zeroCount++;
            }

            double fraction;
            fraction = (double)posCount / (double)arLength;
            Console.WriteLine(fraction.ToString("N6"));
            fraction = (double)negCount / (double)arLength;
            Console.WriteLine(fraction.ToString("N6"));
            fraction = (double)zeroCount / (double)arLength;
            Console.WriteLine(fraction.ToString("N6"));
        }
        
        //Staircase
        static void staircase(int n)
        {
            for (var i = n - 1; i >= 0; i--)
            {
                Console.Write(new string(' ', i));
                Console.WriteLine(new string('#', n - i));
            }
        }

        //Mini-Max Sum
        static void miniMaxSum(int[] arr)
        {
            long sum = 0;

            for (var i = 0; i < arr.Length; i++)
            {
                sum += arr[i];
            }

            long min = sum;
            long max = 0;

            for (var i = 0; i < arr.Length; i++)
            {
                long tmp = sum - arr[i];
                if (tmp > max) max = tmp;
                if (tmp < min) min = tmp;
            }
            Console.WriteLine(min + " " + max);
        }

        //Birthday Cake Candles
        static int birthdayCakeCandles(int[] ar)
        {
            int sum = 0;
            int max = 0;
            for (var i = 0; i < ar.Length; i++)
            {
                if (ar[i] > max)
                {
                    sum = 1;
                    max = ar[i];
                }
                else if (ar[i] == max)
                {
                    sum++;
                }
            }
            return sum;
        }

        //Time Conversion
        static string timeConversion(string s)
        {
            var time = s.Substring(0, s.Length - 2);
            var amPm = s.Substring(s.Length - 2);
            if (amPm == "AM")
            {
                if (time.Substring(0, 2) != "12")
                {
                    return time;
                }
                else
                {
                    return "00" + time.Substring(2);
                }
            }
            else
            {
                var hour = time.Substring(0, 2);
                if (hour == "12")
                {
                    return time;
                }
                else
                {
                    var hournew = (int.Parse(hour) + 12).ToString();
                    return hournew + time.Substring(2);
                }
            }
        }
    }
    class Implementation
    {
        //Grading Students
        static int[] gradingStudents(int[] grades)
        {
            int[] roundedGrades = new int[grades.Length];
            for (var i = 0; i < grades.Length; i++)
            {
                if (grades[i] < 38)
                {
                    roundedGrades[i] = grades[i];
                }
                else if (grades[i] % 5 == 0)
                {
                    roundedGrades[i] = grades[i];
                }
                else
                {
                    var rounded = grades[i];
                    do
                    {
                        rounded++;
                    } while (rounded % 5 != 0);
                    if (rounded - grades[i] < 3)
                    {
                        roundedGrades[i] = rounded;
                    }
                    else
                    {
                        roundedGrades[i] = grades[i];
                    }
                }
            }
            return roundedGrades;
        }

        //Apples and Oranges
        static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges)
        {
            var sumApples = 0;
            for (var i = 0; i < apples.Length; i++)
            {
                if (a + apples[i] >= s && a + apples[i] <= t)
                {
                    sumApples++;
                }
            }

            var sumOranges = 0;
            for (var i = 0; i < oranges.Length; i++)
            {
                if (b + oranges[i] >= s && b + oranges[i] <= t)
                {
                    sumOranges++;
                }
            }

            Console.WriteLine(sumApples);
            Console.WriteLine(sumOranges);
        }

        //Kangaroo
        static string kangaroo(int x1, int v1, int x2, int v2)
        {
            if (x1 != x2 && v1 == v2) return "NO";
            do
            {
                x1 += v1;
                x2 += v2;
                if (x1 == x2) return "YES";
            } while (x1 < x2 && v1 > v2 || x2 < x1 && v2 > v1);
            return "NO";
        }

        //Between Two Sets
        static int getTotalX(int[] a, int[] b)
        {

            var maxb = 0;
            for (var i = 0; i < b.Length; i++)
            {
                if (b[i] > maxb) maxb = b[i];
            }
            var all_A_Factors = new List<int>();
            var all_B_Factors = new List<int>();
            for (var i = 1; i <= maxb; i++)
            {
                var aFactor = true;
                for (var currentA = 0; currentA < a.Length; currentA++)
                {
                    if (i % a[currentA] != 0) aFactor = false;
                }

                if (aFactor) all_A_Factors.Add(i);

                var isFactorOfB = true;
                for (var currentB = 0; currentB < b.Length; currentB++)
                {
                    if (b[currentB] % i != 0) isFactorOfB = false;
                }

                if (isFactorOfB) all_B_Factors.Add(i);
            }

            var similarCount = 0;
            for (var i = 0; i < all_A_Factors.Count; i++)
            {
                for (var j = 0; j < all_B_Factors.Count; j++)
                {
                    if (all_A_Factors[i] == all_B_Factors[j])
                    {
                        similarCount++;
                    }
                }
            }
            return similarCount;
        }

        //Breaking the Records
        static int[] breakingRecords(int[] scores)
        {
            int bestCount = 0, worstCount = 0;
            int bestScore = scores[0];
            int worstScore = bestScore;
            for (var i = 1; i < scores.Length; i++)
            {
                if (scores[i] > bestScore)
                {
                    bestCount++;
                    bestScore = scores[i];
                }
                if (scores[i] < worstScore)
                {
                    worstCount++;
                    worstScore = scores[i];
                }
            }

            return new int[] { bestCount, worstCount };
        }

        //Birthday Chocolate
        static int birthday(List<int> s, int d, int m)
        {
            var possibleCount = 0;
            for (var i = 0; i < s.Count; i++)
            {
                var sum = 0;
                for (var offset = 0; offset < m; offset++)
                {
                    if (i + offset < s.Count)
                    {
                        sum += s[i + offset];
                    }
                    else
                    {
                        sum = -1;
                    }

                }
                if (sum == d) possibleCount++;
            }
            return possibleCount;
        }

        //Divisible Sum Pairs
        static int divisibleSumPairs(int n, int k, int[] ar)
        {
            var divisiblePairs = 0;
            for (var i = 0; i < n - 1; i++)
            {
                for (var j = i + 1; j < n; j++)
                {
                    if ((ar[i] + ar[j]) % k == 0) divisiblePairs++;
                }
            }
            return divisiblePairs;
        }

        //Migratory Birds
        static int migratoryBirds(List<int> arr)
        {
            var birdSightings = new int[5];
            for (var i = 0; i < arr.Count; i++)
            {
                birdSightings[arr[i] - 1]++;
            }

            var mostSightedCount = 0;
            var mostSightedId = 0;
            for (var i = 0; i < birdSightings.Length; i++)
            {
                if (birdSightings[i] > mostSightedCount)
                {
                    mostSightedCount = birdSightings[i];
                    mostSightedId = i + 1;
                }
            }

            return mostSightedId;
        }

        //Day of the Programmer

        static string dayOfProgrammer(int year)
        {
            var monthDayCount = new int[12] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            var leapYearOffset = 0;
            if (year > 1918) //gregorian: leapyear = div by 400, div by 4 but not 100
            {
                if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
                { //Leap year
                    leapYearOffset = 1;
                }
            }
            else if (year < 1918) //julian: leaper = div by 4
            {
                if (year % 4 == 0)
                { //Leap year
                    leapYearOffset = 1;
                }
            }
            else //year == 1918
            {
                leapYearOffset = -13;
            }

            var dayOfYear = 0;
            var month = 0;
            var day = 0;
            for (var i = 0; i < monthDayCount.Length; i++)
            {
                if (dayOfYear <= 256 - monthDayCount[i])
                {
                    dayOfYear += monthDayCount[i];
                }
                else
                {
                    month = i + 1;
                    day = 256 - dayOfYear - leapYearOffset;
                    break;
                }
            }
            return day.ToString("D2") + "." + month.ToString("D2") + "." + year.ToString("D4");
        }

        //Bon Appetit
        static void bonAppetit(List<int> bill, int k, int b)
        {
            var correctCharge = 0;
            for (var i = 0; i < bill.Count; i++)
            {
                if (i != k)
                {
                    correctCharge += bill[i];
                }
            }
            correctCharge /= 2;
            if (correctCharge == b)
            {
                Console.WriteLine("Bon Appetit");
            }
            else
            {
                Console.WriteLine(b - correctCharge);
            }
        }

        //Sock Merchant
        static int sockMerchant(int n, int[] ar)
        {
            var sockCounts = new Dictionary<int, int>();
            for (var i = 0; i < n; i++)
            {
                if (sockCounts.ContainsKey(ar[i]))
                {
                    sockCounts[ar[i]]++;
                }
                else
                {
                    sockCounts.Add(ar[i], 1);
                }
            }
            var totalPairs = 0;
            foreach (var count in sockCounts.Values)
            {
                totalPairs += count / 2;
            }
            return totalPairs;
        }

        //Drawing Book
        static int pageCount(int n, int p)
        {
            var bookEndsOnFrontPage = n % 2 == 0 ? true : false;
            var pageOnFirstHalf = p <= n / 2 ? true : false;
            var pageTurns = 0;

            if (pageOnFirstHalf)
            {
                for (var i = 0; i < n; i += 2)
                {
                    if (p == i || p == i + 1)
                    {
                        break;
                    }
                    pageTurns++;
                }
            }
            else
            {
                for (var i = n; i > 0; i -= 2)
                {
                    if (bookEndsOnFrontPage)
                    {
                        if (p == i || p == i + 1)
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (p == i - 1 || p == i)
                        {
                            break;
                        }
                    }

                    pageTurns++;
                }
            }
            return pageTurns;
        }

        //Counting Valleys
        static int countingValleys(int n, string s)
        {
            var valleysHit = 0;
            var atSeaLevel = true;
            var currentLevel = 0; //Sealevel
                                  //var ar = s.Split();
            foreach (char c in s)
            {
                if (c == 'U')
                {
                    currentLevel++;
                }
                if (c == 'D')
                {
                    currentLevel--;
                    if (atSeaLevel) valleysHit++;
                }
                if (currentLevel == 0)
                {
                    atSeaLevel = true;
                }
                else
                {
                    atSeaLevel = false;
                }
            }
            return valleysHit;
        }

        //Electronics Shop
        static int getMoneySpent(int[] keyboards, int[] drives, int b)
        {
            var highestPrice = -1;
            foreach (var k in keyboards)
            {
                foreach (var d in drives)
                {
                    if (k + d <= b && k + d > highestPrice)
                    {
                        highestPrice = k + d;
                    }
                }
            }

            return highestPrice;
        }

        //Cats and a Mouse
        static string catAndMouse(int x, int y, int z)
        {
            if (Math.Abs(x - z) > Math.Abs(y - z))
            {
                return "Cat B";
            }
            else if (Math.Abs(y - z) > Math.Abs(x - z))
            {
                return "Cat A";
            }
            else
            {
                return "Mouse C";
            }
        }

        //Forming a Magic Square
        static int formingMagicSquare(int[][] s)
        {
            int[][] validMagicSquare = new int[3][] { new int[3] { 2, 7, 6 }, new int[3] { 9, 5, 1 }, new int[3] { 4, 3, 8 } };

            var lowestCost = getCost(s, validMagicSquare);
            int cost;

            //do 3 rotations, 3 checks
            for (var i = 0; i < 3; i++)
            {
                validMagicSquare = rotateSquare(validMagicSquare);
                cost = getCost(s, validMagicSquare);
                if (cost < lowestCost) lowestCost = cost;
            }
            //do 1 mirror, 1 check
            validMagicSquare = flipSquare(validMagicSquare);
            cost = getCost(s, validMagicSquare);

            if (cost < lowestCost) lowestCost = cost;

            //do 3 rotations, 3 checks
            for (var i = 0; i < 3; i++)
            {
                validMagicSquare = rotateSquare(validMagicSquare);
                cost = getCost(s, validMagicSquare);
                if (cost < lowestCost) lowestCost = cost;
            }

            return lowestCost;
        }
        private static int[][] rotateSquare(int[][] s)
        {
            int[][] returnSquare = new int[3][] { new int[3], new int[3], new int[3] };
            returnSquare[2][0] = s[0][0];
            returnSquare[1][0] = s[0][1];
            returnSquare[0][0] = s[0][2];
            returnSquare[2][1] = s[1][0];
            returnSquare[1][1] = s[1][1];
            returnSquare[0][1] = s[1][2];
            returnSquare[2][2] = s[2][0];
            returnSquare[1][2] = s[2][1];
            returnSquare[0][2] = s[2][2];
            return returnSquare;
        }
        private static int[][] flipSquare(int[][] s)
        {
            int[][] returnSquare = new int[3][] { new int[3], new int[3], new int[3] };
            returnSquare[0][2] = s[0][0];
            returnSquare[0][1] = s[0][1];
            returnSquare[0][0] = s[0][2];
            returnSquare[1][2] = s[1][0];
            returnSquare[1][1] = s[1][1];
            returnSquare[1][0] = s[1][2];
            returnSquare[2][2] = s[2][0];
            returnSquare[2][1] = s[2][1];
            returnSquare[2][0] = s[2][2];
            return returnSquare;
        }
        private static int getCost(int[][] s, int[][] v)
        {
            var cost = 0;
            for (var r = 0; r < 3; r++)
            {
                for (var c = 0; c < 3; c++)
                {
                    cost += Math.Abs(s[r][c] - v[r][c]);
                }
            }

            return cost;
        }

    }
}
